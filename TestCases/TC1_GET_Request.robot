*** Settings ***
Library  RequestsLibrary
Library  Collections

*** Variables ***
${base url}  http://restapi.demoqa.com
${city}  Delhi


*** Test Cases ***
Get_weatherInfo
    create session  mySession  ${base_url}
    ${response}=  get request  mySession  utilities/weather/city/${city}

#    log to console  ${response.status_code}
#    log to console  ${response.content}
#    log to console  ${response.headers}

    #validations
    ${status_code}=  convert to string  ${response.status_code}
    should be equal  ${status_code}  200
    log to console  ${status_code}

    ${body}=  convert to string  ${response.content}
    should contain  ${body}  Delhi
    log to console  ${body}

    ${contentTypeValue}=  get from dictionary  ${response.headers}  Content-Type
    should be equal  ${contentTypeValue}  application/json
    log to console  ${contentTypeValue}








