*** Settings ***
Library  RequestsLibrary
Library  Collections

*** Variables ***
${base_url}=  https://restapi.demoqa.com

*** Test Cases ***
BasicAuthTest
    ${auth}=  create list  ToolsQA  TestPassword
    create session  mySession  ${base_url}  auth=${auth}
    ${response}  get request  mySession  /authentication/CheckForAuthentication
    log to console  ${response.content}

    should be equal as strings  ${response.status_code}  200
    log to console  ${response.status_code}





