*** Settings ***
Library  JSONLibrary
Library  os
Library  Collections
Library  RequestsLibrary

*** Variables ***
${base_url}  https://restcountries.eu

*** Test Cases ***
Get_countryInfo
    create_session  mySession  ${base_url}
    ${response}=  get request  mySession  /rest/v2/alpha/IN

    ${json_object}=  to json  ${response.content}

    #single data validation
    ${name_value}=  get value from json  ${json_object}  $.name
    should be equal  ${name_value[0]}  India
    log to console  ${name_value[0]}

    #Single data validation in array
    ${border_value}=  get value from json  ${json_object}  $.borders[0]
    should be equal  ${border_value[0]}  AFG
    log to console  ${border_value[0]}

    #Multi data validation in array
    ${borders_value}=  get value from json  ${json_object}  $.borders
    log to console  ${borders_value[0]}
#    should contain any  ${borders_value[0]}  AFG  BGD  BTN asd
    should not contain any  ${borders_value[0]}  abc  xyz







