*** Settings ***
Library  XML
Library  OS
Library  Collections

*** Test Cases ***
TestCase1
    ${xml_obj}=  parse xml  C:/Users/ramd/Desktop/employees.xml

    #Check the single element value
    #Approach1
    ${emp_firstname}=  get element text  ${xml_obj}  .//employee[1]/firstname
    log to console  ${emp_firstname}
    should be equal  ${emp_firstname}  John

    #Approach2
    ${emp_firstname}=  get element  ${xml_obj}  .//employee[1]/firstname
    should be equal  ${emp_firstname.text}  John

    #Approach3
    element text should be  ${xml_obj}  John  .//employee[1]/firstname

    #Check multiple elements value
    ${count}  get element count  ${xml_obj}  .//employee
    should be equal as integers  ${count}  4

    #Check attributes presence
    element attribute should be  ${xml_obj}  id  1  .//employee[1]

    #Check values of child elements
    ${dzieci}=  get child elements  ${xml_obj}  .//employee[1]
    log to console  ${dzieci}
    should not be empty  ${dzieci}


   ${firstname}  get element text  ${dzieci[0]}
   ${lastname}  get element text  ${dzieci[1]}
   ${title}  get element text  ${dzieci[2]}

   log to console  ${firstname}
   log to console  ${lastname}
   log to console  ${title}

    should be equal  ${firstname}  John
    should be equal  ${lastname}  Doe
    should be equal  ${title}  Engineer


