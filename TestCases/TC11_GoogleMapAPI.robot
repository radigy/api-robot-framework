*** Settings ***
Library  RequestsLibrary
Library  Collections

*** Variables ***
${base_url}=  https://maps.googleapis.com
${req_uri}=  /maps/api/place/nearbysearch/json?

*** Test Cases ***
GoogleMapPlacesAPITC
    create session  mySession  ${base_url}
    ${params}  create dictionary  location=-33.8670522,151.1957362  radius=500  types=food  name=harbour  key=AIzaSyDE6-P4SGTN94b7HdnrsWCVb-E8yaAQHNg
    ${response}=  get request  mySession  ${req_uri}  params=${params}

    log to console  ${response.status_code}
    log to console  ${response.content}

# key = create a new project in google developer console ; create API key for that project ; enable API