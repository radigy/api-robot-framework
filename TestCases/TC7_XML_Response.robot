*** Settings ***
Library  XML
Library  OS
Library  Collections
Library  RequestsLibrary

*** Variables ***
${base_url}=  http://thomas-bayer.com

*** Test Cases ***
TestCase1
    create session  mySession  ${base_url}
    ${response}=  get request  mySession  /sqlrest/CUSTOMER/15
    ${xml_string}=  convert to string  ${response.content}
    ${xml_obj}=  parse xml  ${xml_string}

    # Check single element value
    element text should be  ${xml_obj}  15  .//ID

    # Check multiple values in XML
    ${dzieci}=  get child elements  ${xml_obj}
    log to console  ${dzieci}
    should not be empty  ${dzieci}

   ${id}=  get element text  ${dzieci[0]}
   ${firstname}=  get element text  ${dzieci[1]}
   ${lastname}=  get element text  ${dzieci[2]}
   ${street}=  get element text  ${dzieci[3]}
   ${city}=  get element text  ${dzieci[4]}

    should be equal  ${id}  15
    should be equal  ${firstname}  Bill
    should be equal  ${lastname}  Clancy
    should be equal  ${street}  319 Upland Pl.
    should be equal  ${city}  Seattle







