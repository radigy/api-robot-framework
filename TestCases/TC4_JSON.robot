*** Settings ***
Library  JSONLibrary
Library  os
Library  Collections

*** Test Cases ***
Testcase1:
    ${json_obj}=  load json from file  C:/Users/ramd/Desktop/jsondata.json

    ${name_value}=  get value from json  ${json_obj}  $.firstName
    should be equal  ${name_value[0]}  John

    ${street_value}=  get value from json  ${json_obj}  $.address.streetAddress
    log to console  ${street_value[0]}
    should be equal  ${street_value[0]}  naist street

    ${homeNumber_value}=  get value from json  ${json_obj}  $.phoneNumbers[1].number
    log to console  ${homeNumber_value[0]}
    should be equal  ${homeNumber_value[0]}  0123-4567-8910
