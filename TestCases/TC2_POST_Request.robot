*** Settings ***
Library  RequestsLibrary
Library  Collections

*** Variables ***
${base_url}=   http://restapi.demoqa.com/customer

*** Test Cases ***
Put_CustomerRegistration
    create session  mySession  ${base_url}
    ${body}=  create dictionary  FirstName=aRaj12345  LastName=aRaj12345  UserName=aRaj12345  Password=aRaj12345  Email=aRaj12345@gmail.com
    ${header}=  create dictionary  Content-Type=application/json
    ${response}=  post request  mySession  /register  data=${body}  headers=${header}

    log to console  ${response.status_code}
    log to console  ${response.content}

    #validations

    ${status_code}=  convert to string  ${response.status_code}
    should be equal  ${status_code}  201

    ${res_body}=  convert to string  ${response.content}
    should contain  ${res_body}  OPERATION_SUCCESS
    should contain  ${res_body}  Operation completed successfully


