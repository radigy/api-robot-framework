*** Settings ***
Library  RequestsLibrary
Library  Collections

*** Variables ***
${base_url}  http://localhost:8080

*** Test Cases ***
TC1:Returns all the video games(GET)
    create session  mySession  ${base_url}
    ${response}=  get request  mySession  /app/videogames

    log to console  ${response.status_code}
    log to console  ${response.content}

    #validation

    ${status_code}  convert to string  ${response.status_code}
    should be equal  ${status_code}  200

    ${body}  convert to string  ${response.content}
    should contain  ${body}  Resident
    log to console  ${body}

TC2:Add a new video game (POST)
    create session  mySession  ${base_url}
    ${body}=  create dictionary  id=104  name=Spider-man  releaseDate=2019-10-11T05:11:52.163Z  reviewScore=5  category=test  rating=test
    ${header}=  create dictionary  Content-Type=application/json
    ${response}=  post request  mySession  /app/videogames  data=${body}  headers=${header}

    log to console  ${response.status_code}
    log to console  ${response.content}

    #validations
    ${status_code}=  convert to string  ${response.status_code}
    should be equal  ${status_code}  200

    ${res_body}=  convert to string  ${response.content}
    should contain  ${res_body}  Record Added Successfully

TC3: Returns the details of a single fam by ID(GET)
    create session  mySession  ${base_url}
    ${response}=  get request  mySession  /app/videogames/1

    log to console  ${response.status_code}
    log to console  ${response.content}

    #validation

    ${status_code}  convert to string  ${response.status_code}
    should be equal  ${status_code}  200

    ${body}  convert to string  ${response.content}
    should contain  ${body}  Resident
    log to console  ${body}

TC4:Update an existing video game by specifying a new body (PUT)
    create session  mySession  ${base_url}
    ${body}=  create dictionary  id=104  name=Pacman  releaseDate=2019-10-11T05:11:52.163Z  reviewScore=5  category=test  rating=test
    ${header}=  create dictionary  Content-Type=application/json
    ${response}=  put request  mySession  /app/videogames/104  data=${body}  headers=${header}

    log to console  ${response.status_code}
    log to console  ${response.content}

    #validations
    ${status_code}=  convert to string  ${response.status_code}
    should be equal  ${status_code}  200

    ${res_body}=  convert to string  ${response.content}
    should contain  ${res_body}  Pacman

TC5: Deletes a video game by ID (DELETE)
    create session  mySession  ${base_url}
    ${response}=  delete request  mySession  /app/videogames/104

    log to console  ${response.status_code}
    log to console  ${response.content}

    #validations
    ${status_code}=  convert to string  ${response.status_code}
    should be equal  ${status_code}  200

    ${res_body}=  convert to string  ${response.content}
    should contain  ${res_body}  Record Deleted Successfully











